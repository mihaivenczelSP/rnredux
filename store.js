import { createStore, applyMiddleware } from "redux";

import createSagaMiddleware from 'redux-saga';

import { reducer } from './app/modules/root-reducer';
import { handler as userSaga } from './app/modules/user/sagas'

const sagaMiddleware = createSagaMiddleware();

//The store is created by passing in a reducer
//and has a method called getState
const store = createStore(reducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(userSaga);

export { store };
