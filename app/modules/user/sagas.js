import { takeEvery, put, call } from 'redux-saga/effects';
import { GET_ALL_USER_INFO_REQUEST, GET_ALL_USER_INFO_REQUEST_SUCCESS } from './actions';

// import {queryApi} from '../query-api';

function* handler() {
    yield takeEvery(GET_ALL_USER_INFO_REQUEST, getAllUserInfo);
}

function* getAllUserInfo(action) {
    try {
        // const users = yield call(queryApi, {endpoint: '', method: 'GET' });
        // console.log(users)
        // API call
        yield put({
            type: GET_ALL_USER_INFO_REQUEST_SUCCESS,
            payload: {
                id: '098f6bcd4621d373cade4e832627b4f6',
                name: 'Yo',
                email: 'anothertestemail@test.com',
            },
        });
    } catch (err) {
        console.log(err);
    }
}

export { handler }