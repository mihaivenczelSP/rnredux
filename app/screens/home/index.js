import React, {useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {GET_ALL_USER_INFO_REQUEST} from '../../modules/user/actions';

const mapStateToProps = (state, props) => {
  const {id, name, email} = state.user;

  return {id, name, email};
};

// dispatching actions to the store
const mapDispatchToProps = (dispatch, props) => ({
  getAllUserInfo: () => {
    dispatch({
      type: GET_ALL_USER_INFO_REQUEST,
      payload: {},
    });
  },
});

//Basically, what is seen on the screen
const HomeView = ({id, name, email, getAllUserInfo, navigation}) => {
  // navigation.navigate('Login')
  useEffect(() => {
    getAllUserInfo();
  }, [getAllUserInfo]);

  return (
    <View>
      <Text style={styles.text}>{id}</Text>
      <Text style={styles.text}>Hello, {name}</Text>
      <Text style={styles.text}>{email}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
      textAlign: 'center',
      justifyContent: "center",
      alignItems: "center",
      padding: 28,
      fontSize: 24,
    }
  });
  

const Home = connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeView);

export {Home};