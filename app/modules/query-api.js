import { call, select } from 'redux-saga/effects';
import API_URL from '../utils/config/urls';

function* queryApi({ endpoint, method, body = null }) {
    const state = yield select(); // takes state, in case you need some sort of access token
    const res = yield call(makeRequet, {
        endpoint,
        method,
        headers: { Authorization: state.user.acessToken ? `Bearer ${state.user.acessToken}` : null,
                    'Content-Type': 'application/json',
                 },
        body: JSON.stringify({ ...body }),
    });

    if(res.status === 401) {
        // log the user out
        // explain what it needs in order to log back i,
    }

    const parsedResponse = yield call(parseResponse, res);
    if(!res.ok) {
        // Handle bed response.
        throw new Error;
    };

    return parsedResponse;
}

const makeRequet = async ({ endpoint, method, headers, body = null }) => {
    return fetch(API_URL + endpoint, { 
        method,
        headers,
        body: body === '{}' ? undefined: body
    });
};

const parseResponse = async response => {
    let parsedResponse;

    try {
        parsedResponse = await response.clone().json();
    } catch {
        parsedResponse = await response.text();
    }

    return parsedResponse;
}

export {queryApi}